# PARA LOS AMIGOS DE NAVY LINUX: 
# 
# Para poder insertar emojis en tu sistema y q los reconozca debes instalar estas fuentes de google:
# Desde esta url: https://www.google.com/get/noto/help/install/ al final de la página donde pone "Gnome desktop for all user": Download 
# Descarga el Zip, lo descomprimes, copias y pegas la cartpeta resultante en /usr/share/fonts:
# Una vez hecho esto, ejecuta el siguiente comando para que se actualicen las fuentes del sistema:
# sudo fc-cache -f -v
# Yo te recominedo q después reinicies el sistema para q las aplicaciones refresquen el cache y detecten las fuentes emojis.
# Es todo. El tema es sencillo, manipulando se aprende, cambia el tema a tu gusto o como te dé la gana, esto es Linux y eso significa libertad para compartir y ayudar...
# 
# OBSERVACIONES: Para insertar fuentes puesdes utilizar la siguiente web:  https://emojipedia.org/symbols/
# Sólo tienes que copiar el símbolo y agregarlo desde: Copy and paste this emoji...
# Existe Software para poder explorar las fuentes e insetar símbolos... eso ya es cosa tuya, yo te recomiendo esta forma más fácil que te he explicado antes.
# Saludos!!! Yo3Lb1T............

# Declaración de variables:
# EMOJI es una variable, sólo tienes que cambiar el emoji x otro si no te gusta, copia y pega...

EMOJI=(💀)
SEL_EMOJI=${EMOJI}

# Prompt del sistema que devuelve la Shell:

PROMPT='$fg_bold[blue]┌─[$fg[red]%t $fg_bold[blue]$fg_bold[blue]$SEL_EMOJI $fg[red]%n%:%  $(git_prompt_info)$fg[yellow]$(rvm_prompt_info)$fg_bold[blue]] ➤ [$fg[red]%~$(git_prompt_info)$fg[yellow]$(rvm_prompt_info)$fg_bold[blue]]
└─⏩︎' 



